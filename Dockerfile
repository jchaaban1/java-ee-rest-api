FROM jboss/wildfly:21.0.0.Final

# Set environment variables for directories
ENV DEPLOYMENT_DIR /opt/jboss/wildfly/standalone/deployments/
ENV CONFIG_DIR /opt/jboss/wildfly/standalone/configuration/
ENV MODULES_DIR /opt/jboss/wildfly/modules/

#COPY wildfly/standalone/configuration/standalone.xml ${CONFIG_DIR}

COPY target/todo-app.war ${DEPLOYMENT_DIR}

#COPY wildfly/modules/system/layers/base/org ${MODULES_DIR}/system/layers/base/org/

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]
