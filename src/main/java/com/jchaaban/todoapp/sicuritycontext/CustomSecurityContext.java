package com.jchaaban.todoapp.sicuritycontext;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

public class CustomSecurityContext implements SecurityContext {

    private final SecurityContext originalSecurityContext;
    private final String email;

    public CustomSecurityContext(SecurityContext originalSecurityContext, String email) {
        this.originalSecurityContext = originalSecurityContext;
        this.email = email;
    }

    @Override
    public Principal getUserPrincipal() {
        return () -> email;
    }

    @Override
    public boolean isUserInRole(String role) {
        return originalSecurityContext.isUserInRole(role);
    }

    @Override
    public boolean isSecure() {
        return originalSecurityContext.isSecure();
    }

    @Override
    public String getAuthenticationScheme() {
        return originalSecurityContext.getAuthenticationScheme();
    }
}