package com.jchaaban.todoapp.filter;

import com.jchaaban.todoapp.annotation.AuthenticationRequired;
import com.jchaaban.todoapp.service.security.jwt.JwtTokenService;
import com.jchaaban.todoapp.service.security.jwt.validator.JwtValidatorService;
import com.jchaaban.todoapp.sicuritycontext.CustomSecurityContext;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

@Provider
@AuthenticationRequired
@Priority(1) // the lower the number is the higher the priority
public class SecurityFilter implements ContainerRequestFilter {

    private static final String BEARER_TOKEN_PREFIX = "Bearer ";

    @Inject
    private JwtValidatorService jwtValidator;

    @Inject
    private JwtTokenService jwtTokenService;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
        String token = extractToken(containerRequestContext);

        if (token == null) {
            abortUnauthorized(containerRequestContext);
            return;
        }

        String email = jwtTokenService.extractEmailFromToken(token);


        if (email == null || !validateToken(token, email)) {
            abortUnauthorized(containerRequestContext);
        }

        updateSecurityContext(containerRequestContext, email);

    }

    private void updateSecurityContext(ContainerRequestContext containerRequestContext, String email) {
        SecurityContext originalSecurityContext = containerRequestContext.getSecurityContext();
        SecurityContext customSecurityContext = new CustomSecurityContext(originalSecurityContext, email);
        containerRequestContext.setSecurityContext(customSecurityContext);
    }

    private String extractToken(ContainerRequestContext containerRequestContext) {
        String authorizationHeader = containerRequestContext.getHeaderString(AUTHORIZATION);

        if (authorizationHeader == null || !authorizationHeader.startsWith(BEARER_TOKEN_PREFIX)) {
            return null;
        }

        return authorizationHeader.substring(BEARER_TOKEN_PREFIX.length()).trim();
    }

    private boolean validateToken(String token, String email) {
        try {
            return jwtValidator.validateToken(token, email);
        } catch (Exception e) {
            return false;
        }
    }

    private void abortUnauthorized(ContainerRequestContext containerRequestContext) {
        containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
    }
}
