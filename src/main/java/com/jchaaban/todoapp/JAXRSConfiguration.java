package com.jchaaban.todoapp;

import com.jchaaban.todoapp.exception.mapper.EJBExceptionMapper;
import com.jchaaban.todoapp.exception.mapper.InvalidCredentialsExceptionMapper;
import com.jchaaban.todoapp.filter.SecurityFilter;
import com.jchaaban.todoapp.controller.security.AuthenticationController;
import com.jchaaban.todoapp.controller.todo.TodoController;
import com.jchaaban.todoapp.controller.user.TodoUserController;

import javax.annotation.sql.DataSourceDefinition;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@DataSourceDefinition(
        name = "java:global/postgresDS",
        className = "org.postgresql.ds.PGSimpleDataSource",
        user = "postgres",
        password = "admin",
        url = "jdbc:postgresql://postgres:5432/postgres",
        properties = {
                "ssl=false"
        }
)
@ApplicationPath("/api")
@ApplicationScoped
public class JAXRSConfiguration extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();

        // resource classes
        classes.add(AuthenticationController.class);
        classes.add(TodoController.class);
        classes.add(TodoUserController.class);

        // mapper classes
        classes.add(EJBExceptionMapper.class);
        classes.add(InvalidCredentialsExceptionMapper.class);

        // filter classes
        classes.add(SecurityFilter.class);

        return classes;
    }
}