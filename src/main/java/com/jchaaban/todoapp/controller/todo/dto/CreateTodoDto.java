package com.jchaaban.todoapp.controller.todo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.jchaaban.todoapp.entity.Todo;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateTodoDto {

    private String task;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dueDate;

    private boolean completed;

    public Todo toTodo(){
        return new Todo(task, dueDate, completed);
    }
}
