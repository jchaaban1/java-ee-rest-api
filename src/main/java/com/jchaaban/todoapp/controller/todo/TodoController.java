package com.jchaaban.todoapp.controller.todo;

import com.jchaaban.todoapp.annotation.AuthenticationRequired;
import com.jchaaban.todoapp.entity.Todo;
import com.jchaaban.todoapp.controller.todo.dto.CreateTodoDto;
import com.jchaaban.todoapp.controller.todo.dto.ReadTodoDto;
import com.jchaaban.todoapp.controller.todo.dto.UpdateTodoDto;
import com.jchaaban.todoapp.service.todo.TodoService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/todos")
@AuthenticationRequired
public class TodoController {

    @Inject
    private TodoService todoService;

    @Context
    private SecurityContext securityContext;

    @Path("/create")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTodo(@NotNull CreateTodoDto createTodoDto) {
        String currentUserEmail = securityContext.getUserPrincipal().getName();
        Todo createdTodo = todoService.createTodo(createTodoDto.toTodo(), currentUserEmail);
        return Response.ok(createdTodo.toDto(), MediaType.APPLICATION_JSON).build();
    }

    @Path("/update/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTodo(@NotNull @PathParam("id") UUID id, @NotNull UpdateTodoDto updateTodoDto) {
        Todo updatedTodo = todoService.updateTodoById(updateTodoDto.toTodo(), id);
        return Response.ok(updatedTodo.toDto(), MediaType.APPLICATION_JSON).build();
    }

    @Path("/flipCompletedStatus/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response flipCompletedStatus(@NotNull @PathParam("id") UUID id) {
        Todo updatedTodo = todoService.flipCompletedStatus(id);
        return Response.ok(updatedTodo.toDto(), MediaType.APPLICATION_JSON).build();
    }


    @Path("/fetch/id/{id}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchTodoById(@NotNull @PathParam("id") UUID id) {
        Todo todo = todoService.fetchTodoById(id);
        return Response.ok(todo.toDto(), MediaType.APPLICATION_JSON).build();
    }

    @Path("/fetch/email/{email}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchTodosByEmail(@NotNull @PathParam("email") String email) {
        Todo todo = todoService.fetchTodosByUserEmail(email);
        return Response.ok(todo.toDto(), MediaType.APPLICATION_JSON).build();
    }


    @Path("/all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchAllTodos() {
        List<ReadTodoDto> todoDtoList = todoService.fetchAllTodos().stream().map(Todo::toDto).collect(Collectors.toList());
        return Response.ok(todoDtoList, MediaType.APPLICATION_JSON).build();
    }


    @Path("/completed")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchAllCompletedTodos() {
        List<Todo> todos = todoService.fetchAllCompletedTodos(securityContext.getUserPrincipal().getName());
        List<ReadTodoDto> todoDtoList = todos.stream().map(Todo::toDto).collect(Collectors.toList());
        return Response.ok(todoDtoList, MediaType.APPLICATION_JSON).build();
    }

}
