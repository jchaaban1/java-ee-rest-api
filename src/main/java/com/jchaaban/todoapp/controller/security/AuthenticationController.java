package com.jchaaban.todoapp.controller.security;

import com.jchaaban.todoapp.service.security.authentication.UserAuthenticationService;
import com.jchaaban.todoapp.service.security.jwt.JwtTokenService;

import javax.inject.Inject;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

@Path("/auth")
public class AuthenticationController {

    private static final String BEARER_TOKEN_PREFIX = "Bearer ";

    @Inject
    private JwtTokenService jwtAuthenticationService;
    @Inject
    private UserAuthenticationService userAuthenticationService;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response login(@NotEmpty(message = "Email field must be set") @FormParam("email") String email,
                          @NotEmpty(message = "Password field must be set") @FormParam("password") String password) {

        userAuthenticationService.authenticateAndSetSession(email, password);

        String token = jwtAuthenticationService.generateAndPessimistJwt(email);

        return Response.ok().header(AUTHORIZATION, BEARER_TOKEN_PREFIX + token).build();
    }
}
