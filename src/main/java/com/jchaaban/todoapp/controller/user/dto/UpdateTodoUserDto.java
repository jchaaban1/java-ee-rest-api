package com.jchaaban.todoapp.controller.user.dto;

import com.jchaaban.todoapp.entity.TodoUser;
import lombok.Data;

@Data
public class UpdateTodoUserDto {

    private String name;

    private String email;

    private String password;

    public TodoUser toUser(){
        return new TodoUser(name, email,password);
    }
}
