package com.jchaaban.todoapp.controller.user.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ReadTodoUserDto {

    private UUID id;

    private String name;

    private String email;

    public ReadTodoUserDto(UUID id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }
}
