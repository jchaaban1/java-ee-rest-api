package com.jchaaban.todoapp.controller.user;

import com.jchaaban.todoapp.annotation.AuthenticationRequired;
import com.jchaaban.todoapp.controller.user.dto.CreateTodoUserDto;
import com.jchaaban.todoapp.controller.user.dto.ReadTodoUserDto;
import com.jchaaban.todoapp.controller.user.dto.UpdateTodoUserDto;
import com.jchaaban.todoapp.entity.TodoUser;
import com.jchaaban.todoapp.service.user.TodoUserService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/user")
public class TodoUserController {

    @Inject
    private TodoUserService todoUserService;

    @Path("/create")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTodoUser(@NotNull CreateTodoUserDto createTodoUserDto) {
        TodoUser createdTodoUser = todoUserService.createUser(createTodoUserDto.toUser());
        return Response.ok(createdTodoUser.toDto(), MediaType.APPLICATION_JSON).build();
    }


    @Path("/update/id/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AuthenticationRequired
    public Response updateTodoUserById(@NotNull @PathParam("id") UUID id, @NotNull UpdateTodoUserDto updateTodoUserDto) {
        TodoUser updatedTodoUser = todoUserService.updateUserById(updateTodoUserDto.toUser(), id);
        return Response.ok(updatedTodoUser.toDto(), MediaType.APPLICATION_JSON).build();
    }

    @Path("/update/email/{email}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AuthenticationRequired
    public Response updateTodoUserByEmail(
            @NotNull @PathParam("email") String email,
            @NotNull UpdateTodoUserDto updateTodoUserDto) {
        TodoUser updatedTodoUser = todoUserService.updateUserByEmail(updateTodoUserDto.toUser(), email);
        return Response.ok(updatedTodoUser.toDto(), MediaType.APPLICATION_JSON).build();
    }

    @Path("/fetch/id/{id}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AuthenticationRequired
    public Response fetchTodoUserById(@NotNull @PathParam("id") UUID id) {
        // we can use a default value here using @DefaultValue("")
        TodoUser user = todoUserService.fetchTodoUserById(id);
        return Response.ok(user.toDto(), MediaType.APPLICATION_JSON).build();
    }

    @Path("/fetch/email/{email}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AuthenticationRequired
    public Response fetchTodoUserByEmail(@NotNull @PathParam("email") String email) {
        // we can use a default value here using @DefaultValue("")
        TodoUser user = todoUserService.fetchUserByEmail(email);
        return Response.ok(user.toDto(), MediaType.APPLICATION_JSON).build();

    }

    @Path("/fetch")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AuthenticationRequired
    public Response fetchTodoUsersByName(@NotNull @QueryParam("name") String name) {
        List<TodoUser> users = todoUserService.fetchUsersByName(name);
        List<ReadTodoUserDto> readTodoUserDtoList = users.stream().map(TodoUser::toDto).collect(Collectors.toList());
        return Response.ok(readTodoUserDtoList, MediaType.APPLICATION_JSON).build();
    }

    @Path("/all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @AuthenticationRequired
    public Response fetchAllTodoUsers() {
        List<ReadTodoUserDto> readTodoUserDtoList = todoUserService.fetchAll().stream().map(TodoUser::toDto).collect(Collectors.toList());
        return Response.ok(readTodoUserDtoList, MediaType.APPLICATION_JSON).build();
    }

}
