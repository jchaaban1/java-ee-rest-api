package com.jchaaban.todoapp.service.user.validator;

import com.jchaaban.todoapp.exception.user.InValidPasswordException;

import javax.ejb.Stateless;
import java.util.function.Predicate;

@Stateless
public class PasswordValidator {

    private static final String PASSWORD_REGEX = "^[a-zA-Z0-9!@#$%^&*()-_=+\\[\\]{}|;:'\",.<>?/\\\\]*$";

    private static final Predicate<String> isValid =
            password -> password != null && password.length() >= 8 && password.length() <= 100 && password.matches(PASSWORD_REGEX);

    public void validate(String password){
        if (isValid.test(password)){
            return;
        }

        throw new InValidPasswordException(password);
    }
}
