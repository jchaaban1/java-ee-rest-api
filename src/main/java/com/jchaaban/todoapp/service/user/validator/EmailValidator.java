package com.jchaaban.todoapp.service.user.validator;

import com.jchaaban.todoapp.exception.user.DuplicateEmailException;
import com.jchaaban.todoapp.exception.user.InValidEmailException;
import com.jchaaban.todoapp.service.user.query.TodoUserQueryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.UUID;
import java.util.function.Predicate;

@Stateless
public class EmailValidator {

    @Inject
    private TodoUserQueryService todoUserQueryService;

    private static final Predicate<String> isValidEmailFormat =
            email -> email != null && !email.trim().isEmpty() && email.matches("^[A-Za-z0-9+_.-]+@(.+)$");

    public void validate(String email) {
        validateEmailFormat(email);
        if (isUniqueEmail(email)) {
            return;
        }
        throw new DuplicateEmailException(email);
    }

    public void validate(UUID id, String email) {
        validateEmailFormat(email);
        if (isUniqueEmail(id, email)) {
            return;
        }
        throw new DuplicateEmailException(email);
    }

    private boolean isUniqueEmail(String email) {
        return todoUserQueryService.countTodoUserByEmail(email) == 0;
    }

    private boolean isUniqueEmail(UUID id, String email) {
        return todoUserQueryService.countOtherUsersWithEmail(id, email) == 0;
    }

    private void validateEmailFormat(String email) {
        if (!isValidEmailFormat.test(email)) {
            throw new InValidEmailException(
                    "The email " + email + " is invalid. The email must be in the format user@domain.com and cannot be empty."
            );
        }
    }
}
