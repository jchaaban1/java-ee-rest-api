package com.jchaaban.todoapp.service.user;

import com.jchaaban.todoapp.entity.TodoUser;
import com.jchaaban.todoapp.service.security.authentication.UserCredentialService;
import com.jchaaban.todoapp.service.user.persistence.TodoUserPersistenceService;
import com.jchaaban.todoapp.service.user.validator.TodoUserValidator;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class UserCreationService {

    @Inject
    private TodoUserValidator todoUserValidator;

    @Inject
    private TodoUserPersistenceService todoUserPersistenceService;

    @Inject
    private UserCredentialService passwordHashingService;


    public TodoUser createUser(TodoUser todoUser) {
        todoUserValidator.validateCreatedTodoUser(todoUser);

        passwordHashingService.hashAndSetPasswordCredentials(todoUser);

        return todoUserPersistenceService.persist(todoUser);
    }
}
