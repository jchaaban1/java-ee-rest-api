package com.jchaaban.todoapp.service.user;

import com.jchaaban.todoapp.entity.TodoUser;
import com.jchaaban.todoapp.exception.user.UserEmailNotFoundException;
import com.jchaaban.todoapp.exception.user.UserNotFoundException;
import com.jchaaban.todoapp.service.user.query.TodoUserQueryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

@Stateless
public class UserRetrievalService {

    @Inject
    private TodoUserQueryService todoUserQueryService;


    public List<TodoUser> fetchAll() {
        return todoUserQueryService.fetchAllUsers();
    }

    public TodoUser fetchUserByEmail(String email) {
        return todoUserQueryService.fetchUserByEmail(email).orElseThrow(() -> new UserEmailNotFoundException(email));
    }

    public TodoUser fetchTodoUserById(UUID id) {
        return todoUserQueryService.fetchUserById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    public List<TodoUser> fetchUsersByName(String name) {
        return todoUserQueryService.fetchUsersByName(name);
    }
}
