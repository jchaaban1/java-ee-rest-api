package com.jchaaban.todoapp.service.user.persistence;

import com.jchaaban.todoapp.entity.TodoUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class TodoUserPersistenceService {

    @PersistenceContext(unitName = "todoAppPersistenceUnit")
    private EntityManager entityManager;

    @Transactional
    public TodoUser persist(TodoUser todoUser) {

        entityManager.persist(todoUser);

        return todoUser;
    }

    @Transactional
    public TodoUser merge(TodoUser todoUser) {
        return entityManager.merge(todoUser);
    }
}
