package com.jchaaban.todoapp.service.user;

import com.jchaaban.todoapp.entity.JwToken;
import com.jchaaban.todoapp.entity.TodoUser;
import com.jchaaban.todoapp.service.security.authentication.UserCredentialService;
import com.jchaaban.todoapp.service.security.jwt.JwtTokenService;
import com.jchaaban.todoapp.service.user.persistence.TodoUserPersistenceService;
import com.jchaaban.todoapp.service.user.validator.TodoUserValidator;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.UUID;

@Stateless
public class UserUpdateService {

    @Inject
    private TodoUserValidator todoUserValidator;

    @Inject
    private UserCredentialService passwordHashingService;

    @Inject
    private TodoUserPersistenceService todoUserPersistenceService;

    @Inject
    private UserRetrievalService userRetrievalService;

    @Inject
    private JwtTokenService jwtTokenService;


    public TodoUser updateUserById(TodoUser updatedTodoUser, UUID id) {

        TodoUser existingTodoUser = userRetrievalService.fetchTodoUserById(id);

        todoUserValidator.validateUpdatedTodoUser(updatedTodoUser, id);

        updateJWT(existingTodoUser, updatedTodoUser);

        updateExistingTodoUserWithNewValues(existingTodoUser, updatedTodoUser);

        return todoUserPersistenceService.merge(existingTodoUser);
    }

    public TodoUser updateUserByEmail(TodoUser updatedTodoUser, String email) {
        TodoUser existingTodoUser = userRetrievalService.fetchUserByEmail(email);

        todoUserValidator.validateUpdatedTodoUser(updatedTodoUser, existingTodoUser.getId());

        updateJWT(existingTodoUser, updatedTodoUser);

        updateExistingTodoUserWithNewValues(existingTodoUser, updatedTodoUser);

        return todoUserPersistenceService.merge(existingTodoUser);
    }

    private void updateExistingTodoUserWithNewValues(TodoUser exitingTodoUser, TodoUser updatedTodoUser) {
        exitingTodoUser.setName(updatedTodoUser.getName());
        exitingTodoUser.setEmail(updatedTodoUser.getEmail());
        exitingTodoUser.setPassword(updatedTodoUser.getPassword());
        passwordHashingService.hashAndSetPasswordCredentials(exitingTodoUser);
    }

    private void updateJWT(TodoUser existingTodoUser, TodoUser updatedTodoUser) {
        boolean emailChanged = isEmailChanged(existingTodoUser, updatedTodoUser);
        boolean passwordChanged = isPasswordChanged(existingTodoUser, updatedTodoUser);

        JwToken jwToken = jwtTokenService.fetchJwtByEmail(existingTodoUser.getEmail());

        if (emailChanged) {
            jwtTokenService.updateTokenEmail(jwToken, updatedTodoUser.getEmail());
        }

        if (passwordChanged) {
            jwtTokenService.revokeToken(jwToken);
        }
    }

    private boolean isEmailChanged(TodoUser existingTodoUser, TodoUser updatedTodoUser) {
        String oldUserEmail = existingTodoUser.getEmail();
        String newUserEmail = updatedTodoUser.getEmail();
        return !oldUserEmail.equals(newUserEmail);
    }

    private boolean isPasswordChanged(TodoUser existingTodoUser, TodoUser updatedTodoUser) {
        String oldUserPassword = existingTodoUser.getPassword();

        String newUserPassword = passwordHashingService.hashPassword(
                updatedTodoUser.getPassword(), existingTodoUser.getSalt()
        );

        return !oldUserPassword.equals(newUserPassword);
    }
}
