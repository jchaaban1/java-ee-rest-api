package com.jchaaban.todoapp.service.user.query;

import com.jchaaban.todoapp.entity.TodoUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Stateless
public class TodoUserQueryService {

    @PersistenceContext(unitName = "todoAppPersistenceUnit")
    private EntityManager entityManager;

    public List<TodoUser> fetchAllUsers() {
        TypedQuery<TodoUser> query = entityManager.createNamedQuery(TodoUser.FETCH_ALL_USERS, TodoUser.class);
        return query.getResultList();
    }

    public Optional<TodoUser> fetchUserByEmail(String email) {
        TypedQuery<TodoUser> query = entityManager.createNamedQuery(TodoUser.FETCH_USER_BY_EMAIL, TodoUser.class);
        query.setParameter("email", email);
        return queryResultOrEmpty(query);
    }

    public List<TodoUser> fetchUsersByName(String name) {
        TypedQuery<TodoUser> query = entityManager.createNamedQuery(TodoUser.FETCH_USER_BY_NAME, TodoUser.class);
        query.setParameter("name", "%" + name + "%");
        return query.getResultList();
    }

    // Usage of native queries
    public Long countTodoUserByEmail(String email) {
        return ((BigInteger) entityManager.createNativeQuery(
                        "SELECT COUNT(*) FROM todo_user WHERE email = :email")
                .setParameter("email", email)
                .getSingleResult())
                .longValue();
    }


    public Long countOtherUsersWithEmail(UUID id, String email) {
        return ((BigInteger) entityManager.createNativeQuery(
                        "SELECT COUNT(id) FROM todo_user WHERE email = ?1 AND id != ?2")
                .setParameter(1, email)
                .setParameter(2, id)
                .getSingleResult())
                .longValue();
    }

    public Optional<TodoUser> fetchUserById(UUID id) {
        TypedQuery<TodoUser> query = entityManager.createNamedQuery(TodoUser.FETCH_USER_BY_ID, TodoUser.class);
        query.setParameter("id", id);
        return queryResultOrEmpty(query);
    }

    private Optional<TodoUser> queryResultOrEmpty(TypedQuery<TodoUser> query) {
        try {
            TodoUser user = query.getSingleResult();
            return Optional.of(user);
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }
}
