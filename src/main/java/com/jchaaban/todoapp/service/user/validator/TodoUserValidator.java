package com.jchaaban.todoapp.service.user.validator;

import com.jchaaban.todoapp.entity.TodoUser;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.UUID;

@Stateless
public class TodoUserValidator {

    @Inject
    private NameValidator nameValidator;

    @Inject
    private EmailValidator emailValidator;

    @Inject
    private PasswordValidator passwordValidator;


    public void validateCreatedTodoUser(TodoUser todoUser){
        String email = todoUser.getEmail();
        validateUserData(todoUser.getName(), todoUser.getPassword());
        emailValidator.validate(email);
    }


    public void validateUpdatedTodoUser(TodoUser todoUser, UUID id){
        String email = todoUser.getEmail();
        validateUserData(todoUser.getName(), todoUser.getPassword());
        emailValidator.validate(id, email);
    }

    private void validateUserData(String name, String password) {
        nameValidator.validate(name);
        passwordValidator.validate(password);
    }
}
