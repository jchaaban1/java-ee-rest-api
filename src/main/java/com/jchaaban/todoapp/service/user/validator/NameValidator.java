package com.jchaaban.todoapp.service.user.validator;

import com.jchaaban.todoapp.exception.user.InValidNameException;

import javax.ejb.Stateless;
import java.util.function.Predicate;

@Stateless
public class NameValidator {

    private static final Predicate<String> isValid =
            name -> name != null && !name.trim().isEmpty() && name.length() >= 3 && name.length() <= 50;

    public void validate(String name){
        if (isValid.test(name)){
            return;
        }

        throw new InValidNameException(name);
    }
}
