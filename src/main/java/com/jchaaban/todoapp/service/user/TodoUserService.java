package com.jchaaban.todoapp.service.user;

import com.jchaaban.todoapp.entity.TodoUser;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

@Stateless
public class TodoUserService {

    @Inject
    private UserCreationService userCreationService;

    @Inject
    private UserUpdateService userUpdateService;

    @Inject
    private UserRetrievalService userRetrievalService;


    public TodoUser createUser(TodoUser todoUser) {
        return userCreationService.createUser(todoUser);
    }

    public TodoUser updateUserById(TodoUser updatedTodoUser, UUID id) {
        return userUpdateService.updateUserById(updatedTodoUser, id);
    }

    public TodoUser updateUserByEmail(TodoUser updatedTodoUser, String email) {
        return userUpdateService.updateUserByEmail(updatedTodoUser, email);
    }

    public List<TodoUser> fetchAll() {
        return userRetrievalService.fetchAll();
    }

    public TodoUser fetchUserByEmail(String email) {
        return userRetrievalService.fetchUserByEmail(email);
    }

    public TodoUser fetchTodoUserById(UUID id) {
        return userRetrievalService.fetchTodoUserById(id);
    }

    public List<TodoUser> fetchUsersByName(String name) {
        return userRetrievalService.fetchUsersByName(name);
    }
}
