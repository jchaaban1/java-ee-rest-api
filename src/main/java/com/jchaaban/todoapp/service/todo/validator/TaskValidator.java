package com.jchaaban.todoapp.service.todo.validator;

import com.jchaaban.todoapp.exception.todo.DuplicatedTaskException;
import com.jchaaban.todoapp.exception.todo.InvalidTaskException;
import com.jchaaban.todoapp.service.todo.query.TodoQueryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.UUID;
import java.util.function.Predicate;

@Stateless
public class TaskValidator {

    @Inject
    private TodoQueryService todoQueryService;

    private static final Predicate<String> isNotNullOrEmpty = task -> task != null && !task.trim().isEmpty();
    private static final Predicate<String> isLengthValid = task -> task.length() >= 3 && task.length() <= 140;

    public void validateTaskFormat(String task){
        validate(task, isNotNullOrEmpty, "A todo task must be set");
        validate(task, isLengthValid, "The minimum characters length should be 3 and max 140");
    }

    public void validateTaskUnique(String task){
        if (todoQueryService.countTodoByTask(task) > 0){
            throw new DuplicatedTaskException("The task: " + task + "already exists in this todo");
        }
    }

    public void validateTaskUnique(UUID todoId, String task){
        if (todoQueryService.countTodoByTaskAndTodoId(todoId, task) > 1){
            throw new DuplicatedTaskException("The task: " + task + "already exists in this todo");
        }
    }

    private void validate(String task, Predicate<String> condition, String errorMessage) {
        if (!condition.test(task)) {
            throw new InvalidTaskException(errorMessage);
        }
    }
}
