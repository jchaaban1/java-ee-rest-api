package com.jchaaban.todoapp.service.todo.query;


import com.jchaaban.todoapp.entity.Todo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Stateless
public class TodoQueryService {

    @PersistenceContext(unitName = "todoAppPersistenceUnit") // when we have one persistence unit we don't have to
    private EntityManager entityManager;

    @Context
    private SecurityContext securityContext;

    public List<Todo> fetchAllTodos() {
        return entityManager
                .createNamedQuery(Todo.FETCH_ALL_TODOS, Todo.class)
                .getResultList();
    }

    public Optional<Todo> fetchTodosByEmail(String email) {
        TypedQuery<Todo> query = entityManager
                .createNamedQuery(Todo.FETCH_TODO_BY_USER_EMAIL, Todo.class);
        query.setParameter("email", email);
        return queryResultOrEmpty(query);
    }

    public Optional<Todo> fetchTodoById(UUID id) {
        TypedQuery<Todo> query = entityManager
                .createNamedQuery(Todo.FETCH_TODO_BY_ID, Todo.class);
        query.setParameter("id", id);

        return queryResultOrEmpty(query);
    }

    public List<Todo> fetchAllCompletedTodos(String userEmail) {
        TypedQuery<Todo> query = entityManager
                .createNamedQuery(Todo.FETCH_ALL_COMPLETED_TODOS, Todo.class);

        return query.setParameter("email", userEmail)
                .getResultList();
    }

    public Long countTodoByTask(String task) {
        return ((BigInteger) entityManager.createNativeQuery(
                        "SELECT COUNT(*) FROM Todo WHERE task = :task")
                .setParameter("task", task)
                .getSingleResult())
                .longValue();
    }


    public Long countTodoByTaskAndTodoId(UUID id, String task) {
        return ((BigInteger) entityManager.createNativeQuery(
                        "SELECT COUNT(id) FROM Todo WHERE task = :task AND id != :id")
                .setParameter("task", task)
                .setParameter("id", id)
                .getSingleResult())
                .longValue();
    }

    private Optional<Todo> queryResultOrEmpty(TypedQuery<Todo> query) {
        try {
            Todo todo = query.getSingleResult();
            return Optional.of(todo);
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

}
