package com.jchaaban.todoapp.service.todo.validator;

import com.jchaaban.todoapp.entity.Todo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.time.LocalDate;
import java.util.UUID;

@Stateless
public class TodoValidator {

    @Inject
    private TaskValidator taskValidator;

    @Inject
    private DoDateValidator doDateValidator;


    public void validateCreatedTodo(Todo todo){
        String task = todo.getTask();
        validateTaskAndDueDate(task, todo.getDueDate());
        taskValidator.validateTaskUnique(task);
    }


    public void validateUpdatedTodo(Todo todo, UUID id){
        String task = todo.getTask();
        validateTaskAndDueDate(task, todo.getDueDate());
        taskValidator.validateTaskUnique(id, task);
    }

    private void validateTaskAndDueDate(String task, LocalDate dueDate) {
        doDateValidator.validate(dueDate);
        taskValidator.validateTaskFormat(task);
    }

}
