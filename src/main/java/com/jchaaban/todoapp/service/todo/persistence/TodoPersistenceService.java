package com.jchaaban.todoapp.service.todo.persistence;

import com.jchaaban.todoapp.entity.Todo;
import com.jchaaban.todoapp.entity.TodoUser;
import com.jchaaban.todoapp.service.user.query.TodoUserQueryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class TodoPersistenceService {

    @PersistenceContext(unitName = "todoAppPersistenceUnit")
    private EntityManager entityManager;

    @Inject
    private TodoUserQueryService userQueryService;


    @Transactional
    public Todo persist(Todo todo, String currentUserEmail) {
        TodoUser currentUser = userQueryService.fetchUserByEmail(currentUserEmail).orElse(null);
        todo.setTodoOwner(currentUser);

        entityManager.persist(todo);

        return todo;
    }


    @Transactional
    public Todo merge(Todo todo) {
        return entityManager.merge(todo);
    }

}
