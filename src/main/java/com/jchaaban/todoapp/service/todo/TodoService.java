package com.jchaaban.todoapp.service.todo;

import com.jchaaban.todoapp.entity.Todo;
import com.jchaaban.todoapp.exception.todo.TodoWithEmailNotFoundException;
import com.jchaaban.todoapp.exception.todo.TodoWithIdlNotFoundException;
import com.jchaaban.todoapp.service.todo.persistence.TodoPersistenceService;
import com.jchaaban.todoapp.service.todo.query.TodoQueryService;
import com.jchaaban.todoapp.service.todo.validator.TodoValidator;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Stateless
public class TodoService {

    @Inject
    private TodoQueryService todoQueryService;

    @Inject
    private TodoPersistenceService todoPersistenceService;

    @Inject
    private TodoValidator todoValidator;


    @Transactional
    public Todo createTodo(Todo todo, String currentUserEmail) {
        todoValidator.validateCreatedTodo(todo);
        return todoPersistenceService.persist(todo, currentUserEmail);
    }

    @Transactional
    public Todo updateTodoById(Todo updatedTodo, UUID id) {
        todoValidator.validateUpdatedTodo(updatedTodo, id);

        Todo existingTodo = fetchTodoById(id);

        updateExistingTodoWithNewValues(existingTodo, updatedTodo);

        return todoPersistenceService.merge(existingTodo);
    }

    public Todo fetchTodosByUserEmail(String email) {
        return todoQueryService.fetchTodosByEmail(email)
                .orElseThrow(() -> new TodoWithEmailNotFoundException(email));
    }

    public Todo fetchTodoById(UUID id) {
        return todoQueryService.fetchTodoById(id)
                .orElseThrow(() -> new TodoWithIdlNotFoundException(id));
    }

    public List<Todo> fetchAllTodos() {
        return todoQueryService.fetchAllTodos();
    }

    public List<Todo> fetchAllCompletedTodos(String userEmail) {
        return todoQueryService.fetchAllCompletedTodos(userEmail);
    }

    public Todo flipCompletedStatus(UUID id) {
        Todo existingTodo = fetchTodoById(id);
        flipCompletedStatus(existingTodo);
        return todoPersistenceService.merge(existingTodo);
    }

    private void flipCompletedStatus(Todo existingTodo) {
        boolean isCompleted = existingTodo.isCompleted();
        existingTodo.setCompleted(!isCompleted);
    }

    private void updateExistingTodoWithNewValues(Todo existingTodo, Todo updatedTodo) {
        existingTodo.setTask(updatedTodo.getTask());
        existingTodo.setDueDate(updatedTodo.getDueDate());
        existingTodo.setCompleted(updatedTodo.isCompleted());
    }
}
