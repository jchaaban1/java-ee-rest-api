package com.jchaaban.todoapp.service.todo.validator;

import com.jchaaban.todoapp.exception.todo.InvalidDueDateException;

import javax.ejb.Stateless;
import java.time.LocalDate;
import java.util.function.Predicate;

@Stateless
public class DoDateValidator {

    private static final Predicate<LocalDate> isValid =
            dueDate -> dueDate != null && (dueDate.isAfter(LocalDate.now()) || dueDate.equals(LocalDate.now()));

    public void validate(LocalDate dueDate) {
        if (isValid.test(dueDate)) {
            return;
        }

        throw new InvalidDueDateException(dueDate);
    }


}
