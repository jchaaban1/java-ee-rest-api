package com.jchaaban.todoapp.service.security.jwt.query;

import com.jchaaban.todoapp.entity.JwToken;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Stateless
public class JwtQueryService {

    @PersistenceContext(unitName = "todoAppPersistenceUnit")
    private EntityManager entityManager;


    public Optional<JwToken> findTokenByUUID(String tokenUUID) {
        System.out.println("Token uuid " + tokenUUID);
        TypedQuery<JwToken> query = entityManager.createQuery(
                "SELECT token FROM JwToken token WHERE token.tokenUUID = :tokenUUID AND token.isRevoked = false", JwToken.class);
        query.setParameter("tokenUUID", tokenUUID);
        return queryResultOrEmpty(query);
    }

    public Optional<JwToken> fetchJwtByEmail(String email) {
        TypedQuery<JwToken> query = entityManager.createQuery(
                "SELECT token FROM JwToken token WHERE token.email = :email AND token.isRevoked = false", JwToken.class);
        query.setParameter("email", email);
        return queryResultOrEmpty(query);
    }


    private Optional<JwToken> queryResultOrEmpty(TypedQuery<JwToken> query) {
        try {
            JwToken authToken = query.getSingleResult();
            return Optional.of(authToken);
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

}
