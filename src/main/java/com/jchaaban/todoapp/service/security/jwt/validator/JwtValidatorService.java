package com.jchaaban.todoapp.service.security.jwt.validator;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.jchaaban.todoapp.entity.JwToken;
import com.jchaaban.todoapp.service.security.jwt.JwtTokenService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Stateless
public class JwtValidatorService {

    private static final String SECRET_KEY = "JWT_SECRET_KEY";
    private static final long EXPIRATION_TIME = TimeUnit.DAYS.toMillis(365);

    @Inject
    private JwtTokenService jwtTokenService;


    public boolean validateToken(String token, String email) {
        return verifyEmail(token, email) && tokenNotRevoked(token) && verifyExpiration(token, email);
    }

    private boolean tokenNotRevoked(String token) {
        Optional<JwToken> optToken = jwtTokenService.findTokenByUUID(token);
        return optToken
                .map(JwToken::isRevoked)
                .map(revoked -> !revoked)
                .orElse(false);
    }

    public static boolean verifyEmail(String token, String email) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            JWT.require(algorithm)
                    .withClaim("email", email)
                    .build()
                    .verify(token);
            return true;
        } catch (JWTVerificationException exception) {
            return false;
        }
    }

    public boolean verifyExpiration(String token, String email) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            JWT.require(algorithm)
                    .acceptExpiresAt(EXPIRATION_TIME)
                    .build()
                    .verify(token);
            return true;
        } catch (JWTVerificationException exception) {
            jwtTokenService.revokeTokenHavingEmail(email);
            return false;
        }
    }
}
