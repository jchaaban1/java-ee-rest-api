package com.jchaaban.todoapp.service.security.authentication;

import com.jchaaban.todoapp.entity.TodoUser;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.util.ByteSource;

import javax.ejb.Stateless;

@Stateless
public class UserCredentialService {

    public void hashAndSetPasswordCredentials(TodoUser todoUser) {
        ByteSource salt = generateRandomSalt();
        String hashedPassword = hashPassword(todoUser.getPassword(), salt);

        todoUser.setPassword(hashedPassword);
        todoUser.setSalt(salt.toHex());
    }

    public String hashPassword(String clearTextPassword, String saltText) {
        ByteSource salt = ByteSource.Util.bytes(Hex.decode(saltText));
        return hashPassword(clearTextPassword, salt);
    }

    private String hashPassword(String clearTextPassword, ByteSource salt) {
        return new Sha512Hash(clearTextPassword, salt, 2000000).toHex();
    }

    private ByteSource generateRandomSalt() {
        return new SecureRandomNumberGenerator().nextBytes();
    }
}
