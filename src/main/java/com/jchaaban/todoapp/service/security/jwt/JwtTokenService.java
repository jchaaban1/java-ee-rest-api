package com.jchaaban.todoapp.service.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.jchaaban.todoapp.entity.JwToken;
import com.jchaaban.todoapp.service.security.jwt.persistence.TokenPersistenceService;
import com.jchaaban.todoapp.service.security.jwt.query.JwtQueryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Stateless
public class JwtTokenService {

    private static final String SECRET_KEY = "JWT_SECRET_KEY";
    private static final long EXPIRATION_TIME = TimeUnit.DAYS.toMillis(365); // 365 days

    @Inject
    private JwtQueryService jwtQueryService;

    @Inject
    private TokenPersistenceService jwtPersistenceService;


    public String generateAndPessimistJwt(String email) {
        Date issuedAt = new Date();
        Date expiresAt = new Date(issuedAt.getTime() + EXPIRATION_TIME);

        String authToken = JWT.create()
                .withClaim("email", email)
                .withIssuedAt(issuedAt)
                .withExpiresAt(expiresAt)
                .sign(Algorithm.HMAC256(SECRET_KEY));

        jwtPersistenceService.persist(email, authToken);

        return authToken;
    }

    public String extractEmailFromToken(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("email").asString();
        } catch (Exception e) {
            return null;
        }
    }

    public void updateTokenEmail(JwToken jwToken, String email){
        jwtPersistenceService.updateTokenEmail(jwToken, email);
    }

    public void revokeToken(JwToken jwToken){
        jwtPersistenceService.revoke(jwToken);
    }

    public void revokeTokenHavingEmail(String email) {
        JwToken jwToken = fetchJwtByEmail(email);
        revokeToken(jwToken);
    }

    public Optional<JwToken> findTokenByUUID(String token) {
        return jwtQueryService.findTokenByUUID(token);
    }

    public JwToken fetchJwtByEmail(String email) {
        System.out.println("This is the email we want " + email);
        return jwtQueryService.fetchJwtByEmail(email).orElseThrow(
                () -> new IllegalStateException("JWT not found for email: " + email));
    }
}
