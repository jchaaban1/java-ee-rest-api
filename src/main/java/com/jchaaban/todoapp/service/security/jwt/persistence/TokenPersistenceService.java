package com.jchaaban.todoapp.service.security.jwt.persistence;

import com.jchaaban.todoapp.entity.JwToken;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class TokenPersistenceService {

    @PersistenceContext(unitName = "todoAppPersistenceUnit")
    private EntityManager entityManager;


    @Transactional
    public void persist(String email, String tokenUUID) {
        JwToken tokenRecord = new JwToken(tokenUUID, email);
        entityManager.persist(tokenRecord);
    }

    public void updateTokenEmail(JwToken jwToken, String email){
        jwToken.setEmail(email);
        merge(jwToken);
    }

    @Transactional
    public void revoke(JwToken jwToken) {
        jwToken.setRevoked(true);
        merge(jwToken);
    }

    @Transactional
    private void merge(JwToken jwToken) {
        entityManager.merge(jwToken);
    }
}
