package com.jchaaban.todoapp.service.security.authentication;

import com.jchaaban.todoapp.entity.TodoUser;
import com.jchaaban.todoapp.exception.authentication.InvalidCredentialsException;
import com.jchaaban.todoapp.service.user.query.TodoUserQueryService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.Optional;

@RequestScoped
public class UserAuthenticationService {

    @Inject
    private TodoUserQueryService todoUserQueryService;

    @Inject
    private UserCredentialService passwordHashingService;


    public void authenticateAndSetSession(String email, String password) {
        validateCredentials(email, password);
    }

    private void validateCredentials(String email, String password) {
        if (!validCredentials(email, password)) {
            throw new InvalidCredentialsException();
        }
    }
    private boolean validCredentials(String email, String plainTextPassword) {
        Optional<TodoUser> todoUser = todoUserQueryService.fetchUserByEmail(email);
        return todoUser.filter(user -> passwordsMatch(user.getPassword(), user.getSalt(), plainTextPassword)).isPresent();
    }

    private boolean passwordsMatch(String dbStoredHashedPassword, String salt, String clearTextPassword) {
        String hashedPassword = passwordHashingService.hashPassword(clearTextPassword, salt);
        return hashedPassword.equals(dbStoredHashedPassword);
    }
}
