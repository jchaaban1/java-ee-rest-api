package com.jchaaban.todoapp.exception.user;

import com.jchaaban.todoapp.exception.ApiException;

public class InValidEmailException extends ApiException {

    public InValidEmailException(String message) {
        super(message);
    }
}
