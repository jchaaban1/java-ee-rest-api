package com.jchaaban.todoapp.exception.user;

import com.jchaaban.todoapp.exception.ApiException;

public class InValidPasswordException extends ApiException {
    public InValidPasswordException(String password) {
        super("The password " + password + " is not valid. The Password must be between 3 and 100 characters, and can " +
                "only contain letters, digits, and special characters");
    }
}
