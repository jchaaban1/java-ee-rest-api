package com.jchaaban.todoapp.exception.user;

import com.jchaaban.todoapp.exception.ApiException;

public class InValidNameException extends ApiException {

    public InValidNameException(String name) {
        super("Invalid name " + name + "is invalid. The name must be between 3 and 50 characters and cannot be empty.");
    }
}
