package com.jchaaban.todoapp.exception.user;

import com.jchaaban.todoapp.exception.ApiException;

import java.util.UUID;

public class UserNotFoundException extends ApiException {

    public UserNotFoundException(UUID id) {
        super("The user having ID: " + id + " could not be found");
    }
}
