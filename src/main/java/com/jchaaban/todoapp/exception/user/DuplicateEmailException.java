package com.jchaaban.todoapp.exception.user;

import com.jchaaban.todoapp.exception.ApiException;

public class DuplicateEmailException extends ApiException {

    public DuplicateEmailException(String email) {
        super("The email " + email + " already exists please use another email.");
    }
}