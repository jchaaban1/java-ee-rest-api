package com.jchaaban.todoapp.exception.user;

import com.jchaaban.todoapp.exception.ApiException;

public class UserEmailNotFoundException extends ApiException {

    public UserEmailNotFoundException(String email) {
        super("The user having email: " + email + " could not be found");
    }
}
