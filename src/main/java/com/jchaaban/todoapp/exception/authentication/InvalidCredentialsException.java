package com.jchaaban.todoapp.exception.authentication;

import com.jchaaban.todoapp.exception.ApiException;

public class InvalidCredentialsException extends ApiException {
    public InvalidCredentialsException() {
        super("Either the email or password is invalid");
    }
}
