package com.jchaaban.todoapp.exception.todo;

import com.jchaaban.todoapp.exception.ApiException;

public class TodoWithEmailNotFoundException extends ApiException {
    public TodoWithEmailNotFoundException(String email) {
        super("There is not todo associated with the following email: " + email);
    }
}
