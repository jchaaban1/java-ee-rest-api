package com.jchaaban.todoapp.exception.todo;

import com.jchaaban.todoapp.exception.ApiException;

public class DuplicatedTaskException extends ApiException {

    public DuplicatedTaskException(String message) {
        super(message);
    }
}
