package com.jchaaban.todoapp.exception.todo;

import com.jchaaban.todoapp.exception.ApiException;

public class InvalidTaskException extends ApiException {

    public InvalidTaskException(String message) {
        super(message);
    }
}
