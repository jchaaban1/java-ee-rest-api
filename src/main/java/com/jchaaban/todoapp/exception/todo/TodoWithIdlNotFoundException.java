package com.jchaaban.todoapp.exception.todo;

import com.jchaaban.todoapp.exception.ApiException;

import java.util.UUID;

public class TodoWithIdlNotFoundException extends ApiException {
    public TodoWithIdlNotFoundException(UUID id) {
        super("There is not todo associated with the following ID: " + id.toString());
    }
}
