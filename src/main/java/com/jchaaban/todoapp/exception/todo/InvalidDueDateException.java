package com.jchaaban.todoapp.exception.todo;

import com.jchaaban.todoapp.exception.ApiException;

import java.time.LocalDate;

public class InvalidDueDateException extends ApiException {

    public InvalidDueDateException(LocalDate dueDate) {
        super("The do date " + dueDate +  " you entered isn invalid, this cannot be in the past");
    }
}
