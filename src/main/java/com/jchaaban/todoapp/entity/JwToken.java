package com.jchaaban.todoapp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.UUID;

@Data
@ToString
@Entity
@NoArgsConstructor
@Table(name = "jw_token", indexes = {
        @Index(name = "idx_token_uuid", columnList = "tokenUUID"),
        @Index(name = "idx_revoked_status", columnList = "isRevoked")
})
public class JwToken {

    public JwToken(String tokenUUID, String email) {
        this.tokenUUID = tokenUUID;
        this.email = email;
        this.isRevoked = false;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String tokenUUID;

    private String email;

    private boolean isRevoked;
}