package com.jchaaban.todoapp.entity;

import com.jchaaban.todoapp.controller.user.dto.ReadTodoUserDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@ToString
@NoArgsConstructor
@Table(name = "todo_user")
@NamedQuery(name = TodoUser.FETCH_USER_BY_ID, query = "SELECT user FROM TodoUser user WHERE user.id = :id")
@NamedQuery(name = TodoUser.FETCH_USER_BY_EMAIL, query = "SELECT user FROM TodoUser user WHERE user.email = :email")
@NamedQuery(name = TodoUser.FETCH_ALL_USERS, query = "SELECT user FROM TodoUser user order by user.name")
@NamedQuery(name = TodoUser.FETCH_USER_BY_NAME, query = "SELECT user FROM TodoUser user WHERE user.name LIKE :name")
public class TodoUser {

    public static final String FETCH_USER_BY_ID = "FETCH_USER_BY_ID";
    public static final String FETCH_USER_BY_EMAIL = "FETCH_USER_BY_EMAIL";
    public static final String FETCH_USER_BY_NAME = "FETCH_USER_BY_NAME";
    public static final String FETCH_ALL_USERS = "FETCH_ALL_USERS";


    public TodoUser(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String name;

    private String email;

    private String password;

    private String salt;

    public ReadTodoUserDto toDto(){
        return new ReadTodoUserDto(id, name, email);
    }
}
