package com.jchaaban.todoapp.entity;

import com.jchaaban.todoapp.controller.todo.dto.ReadTodoDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@NamedQuery(name = Todo.FETCH_TODO_BY_ID, query = "SELECT todo FROM Todo todo WHERE todo.id = :id")
@NamedQuery(name = Todo.FETCH_TODO_BY_USER_EMAIL, query = "SELECT todo FROM Todo todo WHERE todo.todoOwner.email = :email")
@NamedQuery(name = Todo.FETCH_ALL_TODOS, query = "SELECT todo FROM Todo todo order by todo.creationDate")
@NamedQuery(name = Todo.FETCH_ALL_COMPLETED_TODOS, query = "SELECT todo from Todo todo WHERE todo.todoOwner.email = :email AND todo.completed = true")
public class Todo {

    public static final String FETCH_TODO_BY_USER_EMAIL = "FETCH_TODO_BY_USER_EMAIL";
    public static final String FETCH_TODO_BY_ID = "FETCH_TODO_BY_USER_ID";
    public static final String FETCH_ALL_TODOS = "FETCH_ALL_TODOS";
    public static final String FETCH_ALL_COMPLETED_TODOS = "FETCH_ALL_COMPLETED_TODOS";

    public Todo(String task, LocalDate dueDate, boolean completed) {
        this.task = task;
        this.dueDate = dueDate;
        this.completed = completed;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String task;

    private LocalDate creationDate;

    private LocalDate dueDate;

    private boolean completed;

    @ManyToOne
    @JoinColumn(name = "todo_user_id")
    private TodoUser todoOwner;

    @PrePersist
    private void init(){
        creationDate = LocalDate.now();
    }

    public ReadTodoDto toDto(){
        return new ReadTodoDto(id, task, dueDate, completed, creationDate);
    }

}
