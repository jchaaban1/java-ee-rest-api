package com.jchaaban.todoapp.service.it;

import com.jchaaban.todoapp.controller.user.dto.CreateTodoUserDto;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;

import static io.restassured.RestAssured.given;

public class TodoBaseIT {

    public static final Long PORT = 8080L;
    public static final String BASE_URL = "http://localhost:" + PORT + "/todo-app/api/";
    public static final int POSTGRES_PORT = 5432;
    public static final String POSTGRES_SERVICE_NAME = "postgres";
    public static final String WILDFLY_SERVICE_NAME = "wildfly";
    public static final int WILDFLY_PORT = 8080;


    protected static String bearerToken;

    protected static DockerComposeContainer testContainers;


    protected static DockerComposeContainer createTestContainers() {
        return new DockerComposeContainer(new File("docker-compose.yml"))
                .withExposedService(POSTGRES_SERVICE_NAME, POSTGRES_PORT, Wait.forListeningPort())
                .withExposedService(WILDFLY_SERVICE_NAME, WILDFLY_PORT, Wait.forHttp("/").forStatusCode(200))
                .withLocalCompose(true);
    }

    protected static Response createUserAndReturnResponse() {
        String createUserUrl = BASE_URL + "user/create";

        CreateTodoUserDto user = new CreateTodoUserDto();
        user.setName("Jaafar Chaaban");
        user.setEmail("test@gmail.com");
        user.setPassword("Password123!");

        return given().contentType(ContentType.JSON)
                .body(user)
                .when()
                .post(createUserUrl);
    }

    protected static Response login() {
        String loginUrl = BASE_URL + "auth/login";
        Response response = given()
                .contentType(ContentType.URLENC)
                .formParam("email", "test@gmail.com")
                .formParam("password", "Password123!")
                .when()
                .post(loginUrl);

        System.out.println("this is the response " + response.asString());

        bearerToken = response.getHeader("Authorization").substring("Bearer ".length());
        return response;
    }




}
