package com.jchaaban.todoapp.service.it;

import com.jchaaban.todoapp.controller.user.dto.UpdateTodoUserDto;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;

@Testcontainers
@TestMethodOrder(OrderAnnotation.class)
public class TodoUserIT extends TodoBaseIT {

    private static UUID todoUserId;

    @BeforeAll
    public static void setup(){
        testContainers = createTestContainers();
        testContainers.start();
    }

    @AfterAll
    public static void tearDown(){
        testContainers.stop();
    }

    @Test
    @Order(1)
    public void createTodoUser() {
        Response response = createUserAndReturnResponse();

        response.then()
                .assertThat()
                .body("name", equalTo("Jaafar Chaaban"))
                .body("email", equalTo("test@gmail.com"))
                .statusCode(200)
                .log().all();

        todoUserId = UUID.fromString(response.path("id"));
    }

    @Test
    @Order(2)
    public void loginSuccessfully() {
        login()
                .then()
                .assertThat()
                .statusCode(200)
                .log().all()
                .extract()
                .response();
    }

    @Test
    @Order(3)
    public void fetchTodoUserByEmail() {
        String userDetailsUrl = BASE_URL + "user/fetch/email/test@gmail.com";

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .contentType("application/json")
                .when()
                .get(userDetailsUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .body("$", not(hasKey("salt")))
                .body("id", equalTo(todoUserId.toString()))
                .body("name", equalTo("Jaafar Chaaban"))
                .body("email", equalTo("test@gmail.com"))
                .log().all();
    }

    @Test
    @Order(4)
    public void fetchAllTodoUsers() {
        String fetchAllUsersUrl = BASE_URL + "user/all";

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .contentType("application/json")
                .when()
                .get(fetchAllUsersUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .body("[0].id", equalTo(todoUserId.toString()))
                .body("[0].name", equalTo("Jaafar Chaaban"))
                .body("[0].email", equalTo("test@gmail.com"))
                .log().all();
    }

    @Test
    @Order(5)
    public void fetchUsersByNameQueryParams() {
        String nameQueryParam = "Jaafar Chaaban";
        String fetchUsersByNameUrl = BASE_URL + "user/fetch?name=" + nameQueryParam;

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .contentType("application/json")
                .when()
                .get(fetchUsersByNameUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .log().all()
                .body("[0].id", equalTo(todoUserId.toString()))
                .body("[0].name", equalTo(nameQueryParam));
    }

    @Test
    @Order(6)
    public void updateUser() {
        UpdateTodoUserDto updateTodoUserDto = new UpdateTodoUserDto();
        updateTodoUserDto.setName("User 123");
        updateTodoUserDto.setEmail("user123@example.com");
        updateTodoUserDto.setPassword("Password1233456");

        String fetchUsersByNameUrl = BASE_URL + "user/update/id/" + todoUserId.toString();

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .contentType("application/json")
                .when()
                .body(updateTodoUserDto)
                .put(fetchUsersByNameUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .log().all()
                .body("id", equalTo(todoUserId.toString()))
                .body("name", equalTo(updateTodoUserDto.getName()))
                .body("email", equalTo(updateTodoUserDto.getEmail()));
    }

}
