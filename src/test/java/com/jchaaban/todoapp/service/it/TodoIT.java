package com.jchaaban.todoapp.service.it;

import com.jchaaban.todoapp.controller.todo.dto.CreateTodoDto;
import com.jchaaban.todoapp.controller.todo.dto.UpdateTodoDto;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;

@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TodoIT extends TodoBaseIT {

    private static UUID todoId;

    @BeforeAll
    public static void setup(){
        testContainers = createTestContainers();
        testContainers.start();
        createUserAndReturnResponse();
        login();
    }

    @AfterAll
    public static void tearDown(){
        testContainers.stop();
    }

    @Test
    @Order(3)
    public void createTodo() {
        String task = "task 3";
        boolean isCompleted = true;
        LocalDate duDate = LocalDate.now().plusDays(3);

        CreateTodoDto createTodoDto = new CreateTodoDto();
        createTodoDto.setTask(task);
        createTodoDto.setDueDate(duDate);
        createTodoDto.setCompleted(isCompleted);


        String createTodoUrl = BASE_URL + "todos/create";

        Response response = given()
                .header("Authorization", "Bearer " + bearerToken)
                .contentType("application/json")
                .body(createTodoDto)
                .when()
                .post(createTodoUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .body("task", equalTo(task))
                .body("dueDate", equalTo(duDate.toString()))
                .body("creationDate", equalTo(LocalDate.now().toString()))
                .body("completed", equalTo(isCompleted))
                .extract().response();

        todoId = UUID.fromString(response.path("id"));

    }

    @Test
    @Order(4)
    public void fetchAllTodos() {
        String task = "task 3";
        boolean isCompleted = true;
        LocalDate dueDate = LocalDate.now().plusDays(3);

        String userDetailsUrl = BASE_URL + "todos/all";

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .when()
                .get(userDetailsUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .body("[0].id", equalTo(todoId.toString()))
                .body("[0].task", equalTo(task))
                .body("[0].dueDate", equalTo(dueDate.toString()))
                .body("[0].creationDate", equalTo(LocalDate.now().toString()))
                .body("[0].completed", equalTo(isCompleted))
                .log().all();
    }

    @Test
    @Order(4)
    public void fetchTodoById() {
        String task = "task 3";
        boolean isCompleted = true;
        LocalDate dueDate = LocalDate.now().plusDays(3);

        String userDetailsUrl = BASE_URL + "todos/fetch/id/" + todoId.toString();

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .when()
                .get(userDetailsUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .body("$", not(hasKey("todoOwner")))
                .body("id", equalTo(todoId.toString()))
                .body("task", equalTo(task))
                .body("dueDate", equalTo(dueDate.toString()))
                .body("creationDate", equalTo(LocalDate.now().toString()))
                .body("completed", equalTo(isCompleted))
                .log().all();
    }

    @Test
    @Order(5)
    public void fetchTodoByEmail() {
        String task = "task 3";
        boolean isCompleted = true;
        LocalDate dueDate = LocalDate.now().plusDays(3);

        String userDetailsUrl = BASE_URL + "todos/fetch/email/test@gmail.com";

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .when()
                .get(userDetailsUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .body("id", equalTo(todoId.toString()))
                .body("task", equalTo(task))
                .body("dueDate", equalTo(dueDate.toString()))
                .body("creationDate", equalTo(LocalDate.now().toString()))
                .body("completed", equalTo(isCompleted))
                .log().all();
    }


    @Test
    @Order(6)
    public void fetchCompletedTodos() {
        String task = "task 3";
        boolean isCompleted = true;
        LocalDate dueDate = LocalDate.now().plusDays(3);

        String userDetailsUrl = BASE_URL + "todos/completed";

        given()
                .header("Authorization", "Bearer " + bearerToken)
                .when()
                .get(userDetailsUrl)
                .then()
                .assertThat()
                .statusCode(200)
                .body("[0].id", equalTo(todoId.toString()))
                .body("[0].task", equalTo(task))
                .body("[0].dueDate", equalTo(dueDate.toString()))
                .body("[0].creationDate", equalTo(LocalDate.now().toString()))
                .body("[0].completed", equalTo(isCompleted))
                .log().all();
    }

    @Test
    @Order(7)
    public void updateTodoById() {
        UpdateTodoDto updateTodoDto = new UpdateTodoDto();
        updateTodoDto.setTask("Update the generated.request.http file");
        updateTodoDto.setDueDate(LocalDate.now().plusDays(3));
        updateTodoDto.setCompleted(false);

        String updateTodoByIdUrl = BASE_URL + "todos/update/" + todoId.toString();

        // Perform the PUT request
        given()
                .header("Authorization", "Bearer " + bearerToken)
                .contentType("application/json")
                .body(updateTodoDto)
                .when()
                .put(updateTodoByIdUrl)
                .then()
                .assertThat()
                .log()
                .all()
                .statusCode(200)
                .body("id", equalTo(todoId.toString()))
                .body("task", equalTo(updateTodoDto.getTask()))
                .body("dueDate", equalTo(updateTodoDto.getDueDate().toString()))
                .body("creationDate", equalTo(LocalDate.now().toString()))
                .body("completed", equalTo(updateTodoDto.isCompleted()));
    }

    @Test
    @Order(8)
    public void flipCompletedStatus() {
        String updateTodoByIdUrl = BASE_URL + "todos/flipCompletedStatus/" + todoId.toString();

        // Perform the PUT request
        given()
                .header("Authorization", "Bearer " + bearerToken)
                .contentType("application/json")
                .when()
                .put(updateTodoByIdUrl)
                .then()
                .assertThat()
                .log()
                .all()
                .statusCode(200)
                .body("completed", equalTo(true));
    }



}
