# Java-ee-rest-api


## Overview

This project demonstrates a Java EE-based RESTful API for managing TODO tasks and user accounts. It features Docker containerization, Wildfly application server, and uses Testcontainers for integration testing.

## Features

### TODO Management
- **Create, Update, and Toggle Completion**: Manage TODO tasks' lifecycle.
- **Retrieve TODOs**: Fetch TODO tasks by ID, user email, or fetch all or completed TODOs.

### User Security Management
- **Create and Update Users**: Manage user accounts by ID or email.
- **Retrieve Users**: Search by ID, email, or fetch all users.

## Technology Stack

### Tools
- **Wildfly**: Java EE application server.
- **PostgreSQL**: Database.
- **Docker**: Containerization for consistent environments.

### Backend
- **Java EE & JAX-RS**: Core technologies for enterprise applications.
- **Hibernate**: Database integration.
- **JWT & Apache Shiro**: Security and authentication.
