#!/bin/bash

# Maven clean and package
mvn clean package -DskipTests

# Docker compose down and up
docker compose down && docker compose up -d --build
